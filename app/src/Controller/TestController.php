<?php
declare(strict_types=1);

namespace App\Controller;

use Random\RandomException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class TestController extends AbstractController
{
    public function index(): Response
    {
        return new Response('Test is listening!');
    }

    /**
     * @throws RandomException
     */
    public function random(): Response
    {
        return new Response('Random number - ' . random_int(1, 1000));
    }
}