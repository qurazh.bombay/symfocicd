FROM php:8.3-fpm-alpine

RUN apk --update upgrade \
    && apk add --no-cache autoconf automake make gcc g++ git bash icu-dev libzip-dev linux-headers

RUN pecl install apcu-5.1.23 && pecl install xdebug-3.3.0

RUN docker-php-ext-install -j$(nproc) \
        bcmath \
        opcache \
        intl \
        zip \
        pdo_mysql

COPY app /var/www/project

WORKDIR /var/www/project

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN curl -sS https://get.symfony.com/cli/installer | bash

RUN composer install \
      --no-interaction \
      --no-plugins \
      --no-scripts \
      --no-autoloader \
      --prefer-dist

COPY php/config/ /usr/local/etc/php/

RUN chown -R root:www-data /var/www && \
    chmod 755 -R /var/www


